﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



public class ConteudoBo
{


    public bool Gravar(Conteudo obj)
    {
        if (obj.Texto != string.Empty && obj.Titulo != string.Empty)
        {
            int sucesso = new ConteudoDao().Gravar(obj);
            if (sucesso != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public IList<Conteudo> ListaConteudo()
    {
        return new ConteudoDao().ListaConteudo();
    }
    public Conteudo Buscar(int idCont)
    {
        return new ConteudoDao().Buscar(idCont);
    }

    public void Excluir(int idCont)
    {
        new ConteudoDao().Excluir(idCont);
    }


}