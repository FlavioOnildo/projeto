﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class UsuarioBo
{

    public bool Gravar(Usuario obj)
    {
        if (obj.Login != string.Empty && obj.Senha != string.Empty)
        {
            int sucesso = new UsuarioDao().Gravar(obj);
            if (sucesso != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public IList<Usuario> ListUsuario()
    {
        return new UsuarioDao().ListUsuario();
    }
    public Usuario Buscar(int idUsuario)
    {
        return new UsuarioDao().Buscar(idUsuario);
    }

    public void Excluir(int idUsuario)
    {
        new ConteudoDao().Excluir(idUsuario);
    }


}