﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TipoDeConteudoBo
{
    public bool Gravar(TipoDeConteudo objTcont)
    {

        if (objTcont.Descricao != string.Empty) 


        {
            int sucesso = new TipoDeConteudoDao().Gravar(objTcont);
            if (sucesso != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    public IList<TipoDeConteudo> ListaTipoConteudo()
{
    return new TipoDeConteudoDao().ListaTipoConteudo();
}
public TipoDeConteudo Buscar(int idTipoCont)
{
    return new TipoDeConteudoDao().Buscar(idTipoCont);
}

public void Excluir(int idTipoCont)
{
        new TipoDeConteudoDao().Excluir(idTipoCont);
}


}