﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

public class ConteudoDao
{
    Database banco = Database.Open("ConexaoBanco");
    public int Gravar(Conteudo obj)
    {
        int sucesso;
        if (obj.IdCont== 0)
        {
            var sql = "Insert Into Conteudo(texto,titulo,data,idTipocont,idUsuario)values(@0,@1,@2,@3,@4)";
            sucesso = banco.Execute(sql, obj.Texto, obj.Titulo,obj.Data,obj.idTipocont,obj.idUsuario);
            var teste = banco.GetLastInsertId();
        }
        else
        {
            var sql = "Update conteudo Set descricao=@0, titulo=@1 Where conteudoId=@5";
            sucesso = banco.Execute(sql, obj.Texto, obj.Titulo, obj.Data, obj.ObjTipoConteudo, obj.idUsuario);
        }

        banco.Close();
        return sucesso;
    }
    public IList<Conteudo> ListaConteudo()
    {
        IList<Conteudo> lista = new List<Conteudo>();
        var sql = "Select * From Conteudo";
        var resultado = banco.Query(sql);
        if (resultado.Count() > 0)
        {
            Conteudo Objconteudo;
            foreach (var item in resultado)
            {
                Objconteudo = new Conteudo
                {
                    IdCont = item.idCont,
                    Texto = item.texto,
                    Titulo = item.titulo,
                    Data = item.data,
                    idTipocont= item.idTipocont,
                    idUsuario = item.idUsuario

                };
                lista.Add(Objconteudo);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return lista;
    }



    public Conteudo Buscar(int idCont)
    {
        var sql = "Select * From Conteudo Where idCont = @0";
        var resultado = banco.QuerySingle(sql, idCont);

            Conteudo Objconteudo;
          
           
                Objconteudo = new Conteudo
                {
                    IdCont=resultado.idcont,
                    Texto=resultado.texto,
                    Titulo=resultado.titulo,
                    Data = resultado.data,
                    idTipocont = resultado.idTipocont,
                    idUsuario = resultado.idUsuario

                };
             
            banco.Close();
            return Objconteudo;

        }

    public void Excluir(int conteudoId)
    {
        var sql = "Delete From Conteudo Where idCont=@0";
        banco.Execute(sql, conteudoId);
        banco.Close();
    }
}
