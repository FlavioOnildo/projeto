﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de TipoDeConteudoDao
/// </summary>
public class TipoDeConteudoDao
{
    Database banco = Database.Open("ConexaoBanco");
    public int Gravar(TipoDeConteudo obj)
    {
        int sucesso;
        if (obj.IdTipoCont == 0)
        {
            var sql = "Insert Into TipoConteudo(descricao)values(@0)";
            sucesso = banco.Execute(sql, obj.Descricao);
            var teste = banco.GetLastInsertId();
        }
        else
        {
            var sql = "Update TipoConteudo Set descricao=@0, titulo=@1 Where conteudoId=@4";
            sucesso = banco.Execute(sql, obj.Descricao);
        }

        banco.Close();
        return sucesso;
    }
    public IList<TipoDeConteudo> ListaTipoConteudo()
    {
        IList<TipoDeConteudo> lista = new List<TipoDeConteudo>();
        var sql = "Select * From TipoConteudo";
        var resultado = banco.Query(sql);
        if (resultado.Count() > 0)
        {
            TipoDeConteudo ObjTipoconteudo;
            foreach (var item in resultado)
            {
                ObjTipoconteudo = new TipoDeConteudo
                {
                    IdTipoCont = item.idTipoCont,
                    Descricao = item.descricao
                };
                lista.Add(ObjTipoconteudo);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return lista;
    }

    public TipoDeConteudo Buscar(int idTipoCont)
    {
        var sql = "Select * From TipoConteudo Where idTipoCont = @0";
        var resultado = banco.QuerySingle(sql, idTipoCont);

        TipoDeConteudo ObjTipoConteudo;


        ObjTipoConteudo = new TipoDeConteudo
        {
            IdTipoCont = resultado.idTipoCont,
            Descricao = resultado.descricao
        };

        banco.Close();
        return ObjTipoConteudo;

    }

    public void Excluir(int idTipoCont)
    {
        var sql = "Delete From TipoConteudo WhereidTipoCont =@0";
        banco.Execute(sql, idTipoCont);
        banco.Close();
    }
}

