﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de UsuarioDao
/// </summary>
public class UsuarioDao
{


    Database banco = Database.Open("ConexaoBanco");
        public int Gravar(Usuario obj)
        {
            int sucesso;
            if (obj.IdUsuario== 0)
            {
                var sql = "Insert Into Usuario(login,senha,nome)values(@0,@1,@2)";
                sucesso = banco.Execute(sql, obj.Login,obj.Senha,obj.Nome);
                var teste = banco.GetLastInsertId();
            }
            else
            {
                var sql = "Update Usuario login=@0, senha=@1 Where idUsuario=@3";
                sucesso = banco.Execute(sql, obj.Login, obj.Senha, obj.Nome);
            }

            banco.Close();
            return sucesso;
        }
        public IList<Usuario> ListUsuario()
        {
            IList<Usuario> lista = new List<Usuario>();
            var sql = "Select * From Usuario";
            var resultado = banco.Query(sql);
            if (resultado.Count() > 0)
            {
                Usuario ObjUsuario;
                foreach (var item in resultado)
                {
                ObjUsuario = new Usuario
                    {
                        IdUsuario =item.idUsuario,
                        Login = item.login,
                        Senha =item.senha,
                        Nome = item.nome

                    };
                    lista.Add(ObjUsuario);
                }
                banco.Close();
            }
            else
            {
                banco.Close();
                return null;
            }
            return lista;
        }

        public Usuario Buscar(int idUsuario)
        {
            var sql = "Select * From  Usuario where idUsuario= @0";
            var resultado = banco.QuerySingle(sql, idUsuario);

        Usuario ObjUsuario;


        ObjUsuario= new Usuario
            {
                IdUsuario = resultado.idUsuario,
                Login = resultado.login,
                Senha=resultado.senha,
                Nome = resultado.nome
            };

            banco.Close();
            return ObjUsuario;

        }

        public void Excluir(int idUsuario)
        {
            var sql = "Delete From Usuario Where idUsuario =@0";
            banco.Execute(sql, idUsuario);
            banco.Close();
        }
    }

