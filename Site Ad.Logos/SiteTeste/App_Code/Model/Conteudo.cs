﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Conteudo
{
    public int IdCont { get; set; }
    public string Texto { get; set; }
    public string Titulo { get; set; }
    public DateTime Data { get; set; }
    public int idTipocont { get; set; }
    public int idUsuario { get; set; }
    public Usuario Objusuario { get; set; }

    public TipoDeConteudo ObjTipoConteudo { get; set; }
}