﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class TipoDeConteudo
{
    public int IdTipoCont { get; set; }
    public string Descricao { get; set; }
    public IList<Conteudo> ListConteudo { get; set; }

}